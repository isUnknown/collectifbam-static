window.addEventListener("DOMContentLoaded", (event) => {
  // NOTES DÉPLIABLES

  const notes = document.querySelectorAll("note")

  for (let i = 0; i < notes.length; i++) {
    let note = notes[i]
    let number = i + 1
    let content = note.textContent
    let isOpen = false

    let mainColor = getComputedStyle(
      document.querySelector(".sidebar")
    ).backgroundColor
    note.style.color = mainColor

    note.textContent = "[" + number + "]"

    note.addEventListener("click", function () {
      if (isOpen === false) {
        note.textContent += " " + content
        isOpen = true
      } else {
        note.textContent = "[" + number + "]"
        isOpen = false
      }
    })
  }

  // PLUS HOME

  // if (document.querySelector('.splashscreen-hometext--custom') !== null) {
  //   const showMore = document.querySelector('.showMore')
  //   let homeText = document.querySelector('.splashscreen-hometext--custom').textContent
  //   let homeTextIsOpen = false

  //   showMore.addEventListener('click', function() {
  //     if (homeTextIsOpen === false) {
  //       document.querySelector('.splashscreen-hometext--custom').style.display = "none"
  //       showMore.textContent = "[-] " + homeText
  //       homeTextIsOpen = true
  //     } else {
  //       showMore.textContent = "[+]"
  //       document.querySelector('.splashscreen-hometext--custom').style.display = "block"
  //       homeTextIsOpen = false
  //     }
  //   })
  // }

  // SIDEBAR / REMPLACEMENT DU PICTO PAR LE LOGO

  const iconContainer = document.querySelector(".sidebar-icon")
  const linkToHome = document.createElement("a")
  const newIcon = document.createElement("img")

  linkToHome.setAttribute("href", "https://www.collectifbam.fr/")
  linkToHome.classList.add("to-home")
  newIcon.setAttribute(
    "src",
    "https://www.collectifbam.fr/assets/images/collectif-bam-logo-blanc.svg"
  )
  newIcon.classList.add("sidebar-logo")

  iconContainer.appendChild(linkToHome)
  linkToHome.appendChild(newIcon)
})

// =========================================== DELETE EMPTY SIDEBAR ELEMENTS (bricolage…)

const sideElems = document.querySelectorAll(".sidebar-meta")

for (let i = 0; i < sideElems.length; i++) {
  if (!sideElems[i].textContent) {
    sideElems[i].style.display = "none"
  }
}
