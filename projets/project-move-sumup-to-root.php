<?php
// Récupérer tous les dossiers de réalisations
$dirs = array_filter(glob('*'), 'is_dir');

// Pour chaque dossiers (réalisation)
foreach ($dirs as $key => $dir) { 

    // Cibler le dossier en question
    $projectDirPath = realpath(dirname(__FILE__)) . '/' . $dir;
    
    // Cibler le dossier 'resume' à l'intérieur
    $resumeDirPath = $projectDirPath . '/resume';

    // Cibler le contenu du dossier 'resume'
    $resumeContentPath = $resumeDirPath . '/*';

    // Déplacer le contenu du dossier 'resume'
    shell_exec("mv $resumeContentPath $projectDirPath");

    // Supprimer le dossier 'resume'
    shell_exec("rm -rf $resumeDirPath");
}
