window.addEventListener("DOMContentLoaded", (event) => {

  // NOTES DÉPLIABLES

  const notes = document.querySelectorAll('note')

  for (let i = 0; i < notes.length; i++) {
    let note = notes[i]
    let number = i+1
    let content = note.textContent
    let isOpen = false

    let mainColor =  getComputedStyle(document.querySelector('.sidebar')).backgroundColor
    note.style.color = mainColor

    note.textContent = "["+number+"]"

    note.addEventListener('click', function() {
      if (isOpen === false) {
        note.textContent += " " + content
        isOpen = true
      } else {
        note.textContent = "["+number+"]"
        isOpen = false
      }
    })
  }


// SIDEBAR / REMPLACEMENT DU PICTO PAR LE LOGO


  // const iconContainer = document.querySelector('.sidebar-icon')
  // const linkToHome = document.createElement('a')
  // const newIcon = document.createElement('img')
  //
  // linkToHome.setAttribute('href', 'https://localhost:8888/')
  // linkToHome.classList.add('to-home')
  // newIcon.setAttribute('src', 'https://localhost:8888/assets/images/collectif-bam-logo-blanc.svg')
  // newIcon.classList.add('sidebar-logo')
  //
  // iconContainer.appendChild(linkToHome)
  // linkToHome.appendChild(newIcon)

});
// =========================================== DELETE EMPTY SIDEBAR ELEMENTS (bricolage…)

const sideElems = document.querySelectorAll('.sidebar-meta')

for (let i = 0; i < sideElems.length; i++) {
  if (!sideElems[i].textContent) {
    sideElems[i].style.display = "none"
  }
}
