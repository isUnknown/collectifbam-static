# Déploiement

### Pré-requis

- hébergement
- domaine collectifbam.fr
- certificat SSL actif

### 1) Télécharger le répertoire

![Capture d'écran téléchargement du répertoire](index_fichiers/download-repo.png)
⚠ ~8Go. Le téléchargement peut prendre un moment.

### 2) Extraire / dézipper l'archive

### 3) Remplacer les URLs locales par les URLs web

Ouvrir le répertoire dans un code editeur puis chercher et remplacer : `http://localhost:888` → `https://collectifbam.fr`

⚠ Respecter exactement les URLs ci-dessus. Le protocole de la première est `http`, celui de la deuxième `https`. Elles n'ont pas de `/` à la fin.

### 4) Tester le projet en local

En utilisant MAMP/WAMP et en mettant bien le projet à la racine du dossier qui contient les projets locaux (il s'appelle probablement localhost ou htdocs). S'il y a déjà des projets dedans, les mettre provisoirement dans un sous-dossier.

### 5) Si ça marche en local : mettre en ligne

Déposer **le contenu** du dossier à la racine de l'hébergement.

### 5b) Si ça ne marche pas en local : me contacter.
